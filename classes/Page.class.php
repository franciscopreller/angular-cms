<?php

	class Page {

		// ==================================================================
		//
		// Properties
		//
		// ------------------------------------------------------------------
		
		private $PageID;
		private $PageAlias;
		private $MenuTitle;
		private $LayoutID;
		private $ParentID;
		private $MenuOrder;
		private $ShowInMenu;
		private $Active;


		/**
		 * Gets the ID of the page
		 *
		 * @return integer The ID of the page
		 */
		public function getPageID() {
		    return $this->PageID;
		}

		/**
		 * Gets the alias of the page
		 *
		 * @return string The alias of the page
		 */
		public function getPageAlias() {
		    return $this->PageAlias;
		}
		
		/**
		 * Sets the alias of the page
		 *
		 * @param String $newPageAlias The alias of the page
		 */
		public function setPageAlias($PageAlias) {
		    $this->PageAlias = $PageAlias;
		
		    return $this;
		}

		/**
		 * Gets the menu title of the page
		 *
		 * @return string The menu title of the page
		 */
		public function getMenuTitle() {
		    return $this->MenuTitle;
		}
		
		/**
		 * Sets the menu title of the page
		 *
		 * @param String $newMenuTitle The menu title of the page
		 */
		public function setMenuTitle($MenuTitle) {
		    $this->MenuTitle = $MenuTitle;
		
		    return $this;
		}

		/**
		 * Gets the layout ID of the page
		 *
		 * @return integer The layout ID of the page
		 */
		public function getLayoutID() {
		    return $this->LayoutID;
		}
		
		/**
		 * Sets the layout ID of the page
		 *
		 * @param Integer $newLayoutID The layout ID of the page
		 */
		public function setLayoutID($LayoutID) {
		    $this->LayoutID = $LayoutID;
		
		    return $this;
		}

		/**
		 * Gets the parent ID of the page
		 *
		 * @return integer The parent ID of the page
		 */
		public function getParentID() {
		    return $this->ParentID;
		}
		
		/**
		 * Sets the parent ID of the page
		 *
		 * @param Integer $newParentID The parent ID of the page
		 */
		public function setParentID($ParentID) {
		    $this->ParentID = $ParentID;
		
		    return $this;
		}

		/**
		 * Gets the menu display order of the page
		 *
		 * @return integer The menu display order of the page
		 */
		public function getMenuOrder() {
		    return $this->MenuOrder;
		}
		
		/**
		 * Sets the menu display order of the page
		 *
		 * @param Integer $newMenuOrder The menu display order of the page
		 */
		public function setMenuOrder($MenuOrder) {
		    $this->MenuOrder = $MenuOrder;
		
		    return $this;
		}

		/**
		 * Gets the show in menu property of the page
		 *
		 * @return boolean True if the page is to appear in the menu
		 */
		public function getShowInMenu() {
		    return $this->ShowInMenu;
		}
		
		/**
		 * Sets the show in menu property of the page
		 *
		 * @param Boolean $newShowInMenu True if the page is to appear in the menu
		 */
		public function setShowInMenu($ShowInMenu) {
		    $this->ShowInMenu = $ShowInMenu;
		
		    return $this;
		}

		/**
		 * Gets the active status of the page
		 *
		 * @return boolean True if the page is active
		 */
		public function getActive() {
		    return $this->Active;
		}
		
		/**
		 * Sets the active status of the page
		 *
		 * @param Boolean $newActive True if the page is active
		 */
		public function setActive($Active) {
		    $this->Active = $Active;
		
		    return $this;
		}

		// ==================================================================
		//
		// Methods
		//
		// ------------------------------------------------------------------
		
		public function __construct($pageAlias) {

			// Check that this page exists - if not, load that 404 error page
			if (!$this->CheckPageExistsByAlias($pageAlias)) {
				$pageAlias = $this->Get404ErrorPageAlias();
			}

			// Create an instance of DAL
			$dal = new DAL();
			
			// Define query to find if page exists
			$sql = "SELECT	PageID, MenuTitle, LayoutID, ParentID, MenuOrder, ShowInMenu, Active
					FROM	page
					WHERE	PageAlias = :PageAlias";
			
			// Define parameters
			$parameters = array(
				array('name' => ':PageAlias', 'value' => $pageAlias, 'type' => PDO::PARAM_STR)
			);
			
			// Execute query
			$result = $dal->executeQuery($sql, $parameters);

			// Extract the row of data from result set
			$result = $result[0];

			// Store data in this object's parameters
			$this->PageID 	  = $result['PageID'];
			$this->PageAlias  = $pageAlias;
			$this->MenuTitle  = $result['MenuTitle'];
			$this->LayoutID   = $result['LayoutID'];
			$this->ParentID   = $result['ParentID'];
			$this->MenuOrder  = $result['MenuOrder'];
			$this->ShowInMenu = $result['ShowInMenu'];
			$this->Active 	  = $result['Active'];
			
		}

		public static function GetDefaultPageAlias() {
			return 'home';
		}

		public static function Get404ErrorPageAlias() {
			return '404';
		}

		/**
		 * Outputs the URL to the website
		 */
		public static function HomePage() {
			echo WEBSITE;
		}

		/**
		 * Checks if a page exists by the alias
		 * @param string $pageAlias The alias of the page to check.
		 * @return boolean True if the page exists.
		 */
		public static function CheckPageExistsByAlias($pageAlias) {

			// Create an instance of DAL
			$dal = new DAL();
			
			// Define query to find if page exists
			$sql = "SELECT	COUNT(PageID)
					FROM	page
					WHERE	PageAlias = :PageAlias";
			
			// Define parameters
			$parameters = array(
				array('name' => ':PageAlias', 'value' => $pageAlias, 'type' => PDO::PARAM_STR)
			);
			
			// Execute query
			$pageCount = $dal->executeScalar($sql, $parameters);
			
			// Check if page exists and return a result
			return ($pageCount > 0);

		}

		/**
		 * Gets the filename and path of the layout used by the page.
		 * @return string The file name and path of the layout.
		 */
		public function GetLayoutFileName() {
			
			// Create an instance of DAL
			$dal = new DAL();
			
			// Define query to find if page exists
			$sql = "SELECT	LayoutFileName
					FROM	layout
					WHERE	LayoutID = :LayoutID";
			
			// Define parameters
			$parameters = array(
				array('name' => ':LayoutID', 'value' => $this->getLayoutID(), 'type' => PDO::PARAM_INT)
			);
			
			// Execute query and return value
			$layoutFileName = $dal->executeScalar($sql, $parameters);

			// Get the absolute path for the layout file
			return sprintf('%1$s/../layouts/%2$s', dirname(__FILE__), $layoutFileName);

		}

		/**
		 * Gets the filename and path of the view template used by the page.
		 * @return string The file name and path of the view.
		 */
		public function GetPageViewTemplate() {

			// Static value for now...
			$pageView = 'client.view.php';

			return sprintf('%1$s/../views/%2$s', dirname(__FILE__), $pageView);
		}

		/**
		 * Gets the value of the specified field for the current page.
		 * @param string $fieldName The name of the field.
		 * @return string The value of the field.
		 */
		public function GetFieldValue($fieldName) {
			
			// Create an instance of DAL
			$dal = new DAL();
			
			// Define query to find if page exists
			$sql = "SELECT	pf.FieldValue
					FROM	page_field pf INNER JOIN field f ON pf.FieldID = f.FieldID
					WHERE	pf.PageID = :PageID
					  AND	f.FieldName = :FieldName";
			
			// Define parameters
			$parameters = array(
				array('name' => ':PageID', 'value' => $this->getPageID(), 'type' => PDO::PARAM_INT),
				array('name' => ':FieldName', 'value' => $fieldName, 'type' => PDO::PARAM_STR)
			);
			
			// Execute query and return value
			return $dal->executeScalar($sql, $parameters);

		}

	}

?>