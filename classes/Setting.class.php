<?php

	require_once 'DAL.class.php';

	if (!class_exists('Setting')) {
		class Setting {

			/**
			 * Gets the value of a setting based on its name.
			 * @param string $settingName The name of the setting.
			 * @return string The value of the setting.
			 */
			public static function GetSettingValue($settingName) {

				$dal = new DAL();

				try {
					
					// Define the query
					$sql = "SELECT SettingValue
							FROM   setting
							WHERE  SettingName = :SettingName";

					// Define the parameters
					$parameters = array(
						array('name' => 'SettingName', 'value' => $settingName, 'type' => PDO::PARAM_STR)
					);

					// Execute the query
					return $dal->executeScalar($sql, $parameters);

				} catch (Exception $e) {
					
					$dal->LogException($e, 'Could not get the setting value.', 'error');
					return false;

				}

			}

			/**
			 * Checks if a setting already exists
			 * @param string $settingName The setting name.
			 * @return boolean True if the setting exists.
			 */
			public static function CheckSettingExists($settingName) {

				$dal = new DAL();

				try {
					
					// Define the query
					$sql = "SELECT COUNT(SettingID)
							FROM   setting
							WHERE  SettingName = :SettingName";

					// Define the parameters
					$parameters = array(
						array('name' => 'SettingName', 'value' => $settingName, 'type' => PDO::PARAM_STR)
					);

					// Execute the query
					$count = $dal->executeScalar($sql, $parameters);

					// Check if setting was found
					return ($count > 0);

				} catch (Exception $e) {
					
					$dal->LogException($e, 'Could not check if the setting exists.', 'error');
					return false;

				}

			}

			/**
			 * Adds a new setting
			 * @param string $settingName  The name of the setting.
			 * @param string $settingValue The value of the setting.
			 * @return boolean True if setting was added successfully.
			 */
			public static function AddSetting($settingName, $settingValue) {

				$dal = new DAL();

				try {
					
					// Define the query
					$sql = "INSERT INTO setting (SettingName, SettingValue)
							VALUES (:SettingName, :SettingValue)";

					// Define the parameters
					$parameters = array(
						array('name' => 'SettingName', 'value' => $settingName, 'type' => PDO::PARAM_STR),
						array('name' => 'SettingValue', 'value' => $settingValue, 'type' => PDO::PARAM_STR)
					);

					// Execute the query
					return $dal->executeNonQuery($sql, $parameters);

				} catch (Exception $e) {
					
					$dal->LogException($e, 'Could not add the setting.', 'error');
					return false;

				}

			}

			/**
			 * Updates existing setting
			 * @param string $settingName  The name of the setting.
			 * @param string $settingValue The vlue of the setting.
			 * @return boolean True if setting was updated successfully.
			 */
			public static function UpdateSetting($settingName, $settingValue) {

				$dal = new DAL();

				try {
					
					// Define the query
					$sql = "UPDATE 	setting
							   SET 	SettingValue = :SettingValue
							 WHERE	SettingName  = :SettingName";

					// Define the parameters
					$parameters = array(
						array('name' => 'SettingName', 'value' => $settingName, 'type' => PDO::PARAM_STR),
						array('name' => 'SettingValue', 'value' => $settingValue, 'type' => PDO::PARAM_STR)
					);

					// Execute the query
					return $dal->executeNonQuery($sql, $parameters);

				} catch (Exception $e) {
					
					$dal->LogException($e, 'Could not add the setting.', 'error');
					return false;

				}

			}

			/**
			 * Deletes a setting.
			 * @param string $settingName The name of the setting to delete.
			 * @return boolean True if setting deleted successfully.
			 */
			public static function DeleteSetting($settingName) {

				$dal = new DAL();

				try {
					
					// Define the query
					$sql = "DELETE FROM setting WHERE SettingName = :SettingName";

					// Define the parameters
					$parameters = array(
						array('name' => 'SettingName', 'value' => $settingName, 'type' => PDO::PARAM_STR)
					);

					// Execute the query
					return $dal->executeNonQuery($sql, $parameters);

				} catch (Exception $e) {
					
					$dal->LogException($e, 'Could not delete the setting.', 'error');
					return false;

				}

			}

			/**
			 * Gets all the settings.
			 * @return array All the settings
			 */
			public static function GetSettings() {

				$dal = new DAL();

				try {
					
					// Define the query
					$sql = "SELECT SettingID, SettingName, SettingValue
							FROM   setting";

					// Execute the query
					return $dal->executeQuery($sql);

				} catch (Exception $e) {
					
					$dal->LogException($e, 'Could not get settings.', 'error');
					return false;

				}

			}

		}
	}

?>