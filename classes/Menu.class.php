<?php

class Menu {

	/**
	 * Generates the navigation structure for the website.
	 * @return string HTML navigation structure.
	 */
	public static function GetNavigation() {
		return Menu::GetNavigationLevel(0);
	}

	public static function GetAdminNavigation() {
		
		// Just set the admin menu manually
		$menu = array(
			array('url' => 'main', 'title' => 'CMS Admin', 'newTab' => false),
			array('url' => 'pages', 'title' => 'Pages', 'newTab' => false),
			array('url' => 'settings', 'title' => 'Settings', 'newTab' => false),
			array('url' => WEBSITE, 'title' => 'View Site', 'newTab' => true)
		);

		return $menu;
	}

	/**
	 * Generates a specific level of the navigation structure.
	 * @param integer $level The navigation level to generate.
	 * @return array Navigation structure
	 */
	private static function GetNavigationLevel($level) {

		// Create an instance of DAL
		$dal = new DAL();
		
		// Define query to find if page exists
		$sql = "SELECT	PageID, PageAlias, MenuTitle, ParentID
				FROM	page
				WHERE	ShowInMenu = 1
				  AND	Active = 1
				  AND	((:ParentID = 0 AND ParentID IS NULL) OR (ParentID = :ParentID))";
		
		// Define parameters
		$parameters = array(
			array('name' => ':ParentID', 'value' => $level, 'type' => PDO::PARAM_INT)
		);
		
		// Execute query and return value
		$result = $dal->executeQuery($sql, $parameters);

		// Loop through each page
		foreach ($result as $page) {

			// Get page info
			$pageID    = $page['PageID'];
			$pageAlias = $page['PageAlias'];
			$menuTitle = $page['MenuTitle'];
			$parentID  = $page['ParentID'];

			// Does it require a dropdown?
			$dropdown  = Menu::HasChildren($pageID);

			// Get the URL
			$url       = $pageAlias === Page::GetDefaultPageAlias() ? WEBSITE : WEBSITE . $pageAlias;

			// Add menu item to output
			$output[]  = array('url' => $url, 'dropdown' => $dropdown, 'title' => $menuTitle);

			// Check if page has children
			if (Menu::HasChildren($pageID)) {

				// Get last $output key
				end($output);
				$lastID = key($output);

				// Get all sub-pages of the current page (all children)
				$output[$lastID]['children'] = Menu::GetNavigationLevel($pageID);

				// Prepend the current page alias as a menu item.
				// This is to counter the fact that bootstrap does not natively click the initial menu item
				$menuTitle = ucwords(str_replace('-', ' ', $pageAlias));
				array_unshift($output[$lastID]['children'], array('url' => $pageAlias, 'dropdown' => false, 'title' => $menuTitle));

			}

		}

		// End the output of the list and return the result
		return $output;

	}

	/**
	 * Checks if a page has child pages.
	 * @param integer $pageID The page ID.
	 * @return boolean True if the page has child pages.
	 */
	private static function HasChildren($pageID) {

		// Create an instance of DAL
		$dal = new DAL();
		
		// Define query to find if page exists
		$sql = "SELECT	COUNT(PageID)
				FROM	page
				WHERE	ParentID = :ParentID";
		
		// Define parameters
		$parameters = array(
			array('name' => ':ParentID', 'value' => $pageID, 'type' => PDO::PARAM_INT)
		);
		
		// Execute query
		$pageCount = $dal->executeScalar($sql, $parameters);
		
		// Check if page exists and return a result
		return ($pageCount > 0);

	}

}

?>