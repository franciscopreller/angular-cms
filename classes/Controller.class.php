<?php

class Controller {
	
	// ==================================================================
	//
	// Properties
	//
	// ------------------------------------------------------------------

	private $method;
	private $id       = null;
	private $response = false;
	private $params   = array();

	const HTTP_GET    = 'GET_METHOD';
	const HTTP_POST   = 'POST_METHOD';
	const HTTP_PUT    = 'PUT_METHOD';
	const HTTP_DELETE = 'DELETE_METHOD';

	/**
	 * Gets the HTTP method
	 * @return string The HTTP Method
	 */
	public function getMethod() {
		return $this->method;
	}

	/**
	 * Gets the Controller response to the view
	 * @return Object The controller response
	 */
	public function getResponse() {
	    return $this->response;
	}
	
	/**
	 * Sets the Controller response to the view
	 * @param Object $newresponse The controller response
	 */
	public function setResponse($response) {
	    $this->response = $response;
	}


	/**
	 * Gets the $_POST params passed to the controller
	 * @return array The $_POST parameters
	 */
	public function getParams() {
	    return $this->params;
	}
	
	/**
	 * Sets the $_POST parameters passed to the controller
	 * @param Array $newParams The $_POST parameters
	 */
	public function setParams($params) {
	    $this->params = $params;
	}

	/**
	 * Gets the main arguments
	 * @param integer The index of the specific argument to get, leave empty for all
	 * @return array The controller arguments
	 */
	public function getArgs($index = null) {
	    return is_null($index) ? $this->args : $this->args[$index];
	}
	
	/**
	 * Sets the main arguments
	 * @param Array $newArgs The controller arguments
	 */
	public function setArgs($args) {
	    $this->args = $args;
	}
	

	// ==================================================================
	//
	// Methods
	//
	// ------------------------------------------------------------------
	

	public function __construct($args = array()) {

		// Set method
		$this->SetHTTPMethod();

		// Set incoming array data
		$data    = file_get_contents("php://input");
		$objData = json_decode($data);
		$this->setParams($objData);

		// Set ID
		$this->setArgs($args);
	}

	public function HasArgs() {
		return (count($this->getArgs()) > 0);
	}

	/**
	 * Determines if the request method is of the GET type
	 * @return boolean True if the request method is of the GET type.
	 */
	public function IsGet() {
		return ($this->method === self::HTTP_GET);
	}

	/**
	 * Determines if the request method is of the POST type
	 * @return boolean True if the request method is of the POST type.
	 */
	public function IsPost() {
		return ($this->method === self::HTTP_POST);
	}

	/**
	 * Determines if the request method is of the PUT type
	 * @return boolean True if the request method is of the PUT type
	 */
	public function IsPut() {
		return ($this->method === self::HTTP_PUT);
	}

	/**
	 * Determines if the request method is of the DELETE type
	 * @return boolean True if the request method is of the DELETE type
	 */
	public function IsDelete() {
		return ($this->method === self::HTTP_DELETE);
	}

	/**
	 * Gets the JSON response from the controller and method, then
	 * sends it back to the caller after sending JSON headers.
	 */
	public function GetJsonResponse() {

		// Get the response data
		$data = $this->getResponse();

		// Send the headers
		$this->SendJsonHeaders();

		// Send the json data and exit execution
		die(json_encode($data));

	}

	/**
	 * Sends JSON headers to the client.
	 */
	private function SendJsonHeaders() {
		header('Content-Type: application/json');
	}

	/**
	 * Sets the request method internally, this can be used for
	 * checking which REST method needs to be used.
	 */
	private function SetHTTPMethod() {

		// Set the method variable
		$method = $_SERVER['REQUEST_METHOD'];

		switch ($method) {
			case 'GET': 	$this->method = self::HTTP_GET; 	break;
			case 'POST': 	$this->method = self::HTTP_POST; 	break;
			case 'PUT':	 	$this->method = self::HTTP_PUT; 	break;
			case 'DELETE': 	$this->method = self::HTTP_DELETE; 	break;
		}

	}


}

?>