<?php

class Validation {

	/**
	 * Checks that a field in the database is unique.
	 * Note that the $dbField param must be passed in the following format: Table.Field
	 * @param string $dbField The table & field for the database.
	 * @param string $value   The value to check in the table.
	 */
	public static function IsUnique($dbField, $value) {

		// Separate the field and the table
		$fieldArray = explode('.', $dbField, 2);
		$tableName  = strtolower($fieldArray[0]);
		$fieldName  = $fieldArray[1];

		$dal = new DAL();

		try {
			
			// Define the query
			$sql = "SELECT COUNT({$fieldName})
					FROM   {$tableName}
					WHERE  {$fieldName} = :Value";

			// Define the parameters
			$parameters = array(
				array('name' => 'Value', 'value' => $value, 'type' => PDO::PARAM_STR)
			);

			// Execute the query
			$count = $dal->executeScalar($sql, $parameters);

			// Check if value was found
			return !($count > 0);

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not get the setting value.', 'error');
			return false;

		}

	}

}

?>