<?php

class Pages {

	/**
	 * gets a single page by page alias
	 * @param string $pageAlias The page Alias
	 */
	public static function GetPage($pageAlias) {

		// Create an instance of DAL
		$dal = new DAL();

		// Define query to find if page exists
		$sql = "SELECT	page.PageID,
						page.PageAlias,
						page.MenuTitle,
						page.LayoutID,
						page.ParentID,
						page.MenuOrder,
						page.ShowInMenu,
						page.Active
				FROM	page
				WHERE	PageAlias = :PageAlias";

		// Define parameters
		$parameters = array(
			array('name' => ':PageAlias', 'value' => $pageAlias, 'type' => PDO::PARAM_STR)
		);
		
		// Execute query
		$result = $dal->executeQuery($sql, $parameters);

		return $result[0];
	}

	/**
	 * Gets all pages with additional information
	 */
	public static function GetPages() {

		// Create an instance of DAL
		$dal = new DAL();

		// Define query to find if page exists
		$sql = "SELECT	 page.PageID,
						 page.PageAlias,
						 page.MenuTitle,
						 page.LayoutID,
						 layout.LayoutName,
						 @pid:=page.ParentID AS 'ParentID',
						 (SELECT MenuTitle FROM page WHERE PageID = @pid) AS 'ParentMenuTitle',
						 page.MenuOrder,
						 IF(page.ShowInMenu = 1, 'Shown', 'Hidden') AS 'ShowInMenu',
						 IF(page.Active = 1, 'Active', 'Inactive') AS 'Active'
				FROM	 page
						 JOIN layout ON page.LayoutID = layout.LayoutID
				ORDER BY ParentMenuTitle, page.MenuOrder";
		
		// Execute query
		return $dal->executeQuery($sql);
	}

	/**
	 * Gets all layouts
	 */
	public static function GetLayouts() {

		// Create an instance of DAL
		$dal = new DAL();

		// Define query to find if page exists
		$sql = "SELECT	 LayoutID, LayoutName
				FROM	 layout";
		
		// Execute query
		return $dal->executeQuery($sql);

	}

	/**
	 * Gets all parent pages
	 */
	public static function GetParents() {

		// Create an instance of DAL
		$dal = new DAL();

		// Define query to find if page exists
		$sql = "SELECT	PageID AS 'ParentID', MenuTitle AS 'ParentMenuTitle'
				FROM	page
				WHERE	ParentID IS NULL";
		
		// Execute query
		return $dal->executeQuery($sql);

	}

	/**
	 * Gets all page fields and values for a page by its ID
	 * @param integer $pageID The page ID
	 */
	public static function GetPageFields($pageID) {

		// Create an instance of DAL
		$dal = new DAL();

		// Define query to find if page exists
		$sql = "SELECT	page_field.FieldValue, page_field.FieldID, field.FieldName, field.FieldType
				FROM	page_field, field
				WHERE	page_field.FieldID = field.FieldID
				  AND	page_field.PageID = :PageID";
		
		// Define parameters
		$parameters = array(
			array('name' => ':PageID', 'value' => $pageID, 'type' => PDO::PARAM_INT)
		);
		
		// Execute query
		return $dal->executeQuery($sql, $parameters);

	}

	/**
	 * [AddPage description]
	 * @param [type] $params [description]
	 */
	public static function AddPage($params) {

		$dal = new DAL();

		try {

			// Check for parentID and if its not set, set to null manually
			if (!isset($params->ParentID))
				$params->ParentID = null;
			
			// Define the query
			$sql = "INSERT INTO page (PageAlias, LayoutID, MenuOrder, MenuTitle, Active, ParentID, ShowInMenu)
						 VALUES (:PageAlias, :LayoutID, :MenuOrder, :MenuTitle, :Active, :ParentID, :ShowInMenu)";

			// Define the parameters
			$parameters = array(
				array('name' => 'PageAlias', 'value' => $params->PageAlias, 'type' => PDO::PARAM_STR),
				array('name' => 'LayoutID', 'value' => $params->LayoutID, 'type' => PDO::PARAM_INT),
				array('name' => 'MenuOrder', 'value' => $params->MenuOrder, 'type' => PDO::PARAM_INT),
				array('name' => 'MenuTitle', 'value' => $params->MenuTitle, 'type' => PDO::PARAM_STR),
				array('name' => 'Active', 'value' => $params->Active, 'type' => PDO::PARAM_INT),
				array('name' => 'ParentID', 'value' => $params->ParentID, 'type' => PDO::PARAM_INT),
				array('name' => 'ShowInMenu', 'value' => $params->ShowInMenu, 'type' => PDO::PARAM_INT)
			);

			// Execute the query
			$dal->executeNonQuery($sql, $parameters);

			// Get the inserted ID
			return $dal->getLastInsertID();

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not add new page.', 'error');
			return false;

		}

	}

	/**
	 * Updates the page parameters
	 * @param object $params The parameters
	 */
	public static function UpdatePage($params) {

		$dal = new DAL();

		try {
			
			// Define the query
			$sql = "UPDATE 	page
					   SET 	PageAlias  = :PageAlias,
					   		LayoutID   = :LayoutID,
					   		MenuOrder  = :MenuOrder,
					   		MenuTitle  = :MenuTitle,
					   		Active     = :Active,
							ParentID   = :ParentID,
							ShowInMenu = :ShowInMenu
					 WHERE	PageID     = :PageID";

			// Define the parameters
			$parameters = array(
				array('name' => 'PageAlias', 'value' => $params->PageAlias, 'type' => PDO::PARAM_STR),
				array('name' => 'LayoutID', 'value' => $params->LayoutID, 'type' => PDO::PARAM_INT),
				array('name' => 'MenuOrder', 'value' => $params->MenuOrder, 'type' => PDO::PARAM_INT),
				array('name' => 'MenuTitle', 'value' => $params->MenuTitle, 'type' => PDO::PARAM_STR),
				array('name' => 'Active', 'value' => $params->Active, 'type' => PDO::PARAM_INT),
				array('name' => 'ParentID', 'value' => $params->ParentID, 'type' => PDO::PARAM_INT),
				array('name' => 'PageID', 'value' => $params->PageID, 'type' => PDO::PARAM_INT),
				array('name' => 'ShowInMenu', 'value' => $params->ShowInMenu, 'type' => PDO::PARAM_INT)
			);

			// Execute the query
			return $dal->executeNonQuery($sql, $parameters);

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not update the page.', 'error');
			return false;

		}

	}

	/**
	 * Iterates over all fields, and either updates or creates them
	 * @param object $fields  All fields which belong to page
	 * @param integer $pageID The pageID of the page
	 */
	public static function AddPageFields($fields, $pageID) {

		// Iterate over all fields
		foreach ($fields as $field) {

			if (!is_null($field->FieldValue)) {

				// Check if the field exists already
				if (Pages::CheckPageFieldExists($pageID, $field->FieldID)) {

					// If it does, update existing
					if (!Pages::UpdateField($pageID, $field->FieldID, $field->FieldValue))
						return false;

				} else {

					// If it doesn't, create a new page field
					if (!Pages::AddField($pageID, $field->FieldID, $field->FieldValue))
						return false;

				}

			}
		}

		return true;

	}

	/**
	 * Updates an existing page field
	 * @param integer $pageID     The PageID of the page field
	 * @param integer $fieldID    The FieldID of the page field
	 * @param string $fieldValue  The field value of the page field
	 */
	public static function UpdateField($pageID, $fieldID, $fieldValue) {

		$dal = new DAL();

		try {
			
			// Define the query
			$sql = "UPDATE 	page_field
					   SET 	FieldValue = :FieldValue
					 WHERE	PageID     = :PageID
					   AND  FieldID    = :FieldID";

			// Define the parameters
			$parameters = array(
				array('name' => 'FieldValue', 'value' => $fieldValue, 'type' => PDO::PARAM_STR),
				array('name' => 'PageID', 'value' => $pageID, 'type' => PDO::PARAM_INT),
				array('name' => 'FieldID', 'value' => $fieldID, 'type' => PDO::PARAM_INT)
			);

			// Execute the query
			return $dal->executeNonQuery($sql, $parameters);

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not update the page field.', 'error');
			return false;

		}

	}

	/**
	 * Adds a new page field
	 * @param integer $pageID     The PageID of the page field
	 * @param integer $fieldID    The FieldID of the page field
	 * @param string $fieldValue  The value of the page field
	 */
	public static function AddField($pageID, $fieldID, $fieldValue) {

		$dal = new DAL();

		try {
			
			// Define the query
			$sql = "INSERT INTO page_field (PageID, FieldID, FieldValue)
					VALUES (:PageID, :FieldID, :FieldValue)";

			// Define the parameters
			$parameters = array(
				array('name' => 'PageID', 'value' => $pageID, 'type' => PDO::PARAM_INT),
				array('name' => 'FieldID', 'value' => $fieldID, 'type' => PDO::PARAM_INT),
				array('name' => 'FieldValue', 'value' => $fieldValue, 'type' => PDO::PARAM_STR)
			);

			// Execute the query
			return $dal->executeNonQuery($sql, $parameters);

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not add the page field.', 'error');
			return false;

		}

	}

	/**
	 * Checks if a page field exists already
	 * @param integer $pageID  The PageID of the page
	 * @param integer $fieldID The FieldID of the field
	 */
	public static function CheckPageFieldExists($pageID, $fieldID) {

		$dal = new DAL();

		try {
			
			// Define the query
			$sql = "SELECT COUNT(*)
					FROM   page_field
					WHERE  PageID  = :PageID
					  AND  FieldID = :FieldID";

			// Define the parameters
			$parameters = array(
				array('name' => 'PageID', 'value' => $pageID, 'type' => PDO::PARAM_INT),
				array('name' => 'FieldID', 'value' => $fieldID, 'type' => PDO::PARAM_INT)
			);

			// Execute the query
			$count = $dal->executeScalar($sql, $parameters);

			// Check if setting was found
			return ($count > 0);

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not check if the page field exists.', 'error');
			return false;

		}

	}

}

?>