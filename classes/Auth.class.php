<?php

class Auth {

	// ==================================================================
	//
	// Properties
	//
	// ------------------------------------------------------------------

	/**
	 * Sets the user ID in the session variable
	 * @param integer $id The user ID.
	 */
	public static function setID($id) {
		$_SESSION['userLoggedIn'] = $id;
	}

	/**
	 * Unsets the user IS session variable
	 */
	public static function unsetID() {
		unset($_SESSION['userLoggedIn']);
	}

	/**
	 * Checks session to see logged in state and returns the state.
	 */
	public static function LoggedIn() {

		return isset($_SESSION['userLoggedIn']) ? true : false;

	}

	// ==================================================================
	//
	// Methods
	//
	// ------------------------------------------------------------------
	

	/**
	 * Authenticates user by username and password.
	 * @param string $username The username.
	 * @param string $password The password.
	 */
	public static function AuthenticateUser($username, $password) {

		$dal = new DAL();

		try {
			
			// Define the query
			$sql = "SELECT COUNT(UserID)
					FROM   user
					WHERE  Username = :Username
					  AND  Password = :Password";

			// Define the parameters
			$parameters = array(
				array('name' => 'Username', 'value' => $username, 'type' => PDO::PARAM_STR),
				array('name' => 'Password', 'value' => $password, 'type' => PDO::PARAM_STR)
			);

			// Execute the query
			$count = $dal->executeScalar($sql, $parameters);

			// Check if setting was found and is exactly 1
			return ($count > 0);

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not check for user authentication.', 'error');
			return false;

		}

	}

	/**
	 * Gets the user ID and sets it into the session variable
	 * @param string $username The username.
	 * @param string $password The password.
	 */
	public static function LoginUserAndSetID($username, $password) {

		$dal = new DAL();

		try {
			
			// Define the query
			$sql = "SELECT UserID
					FROM   user
					WHERE  Username = :Username
					  AND  Password = :Password";

			// Define the parameters
			$parameters = array(
				array('name' => 'Username', 'value' => $username, 'type' => PDO::PARAM_STR),
				array('name' => 'Password', 'value' => $password, 'type' => PDO::PARAM_STR)
			);

			// Execute the query
			$userID = $dal->executeScalar($sql, $parameters);

			// Set user ID in login sessions state
			Auth::setID($userID);

			return true;

		} catch (Exception $e) {
			
			$dal->LogException($e, 'Could not check for user authentication.', 'error');
			return false;

		}

	}

	/**
	 * Logs user out and unsets the session variable
	 */
	public static function LogoutUserAndEndSession() {

		// Check if user is logged in
		if (Auth::LoggedIn()) {

			Auth::unsetID();
			session_destroy();

		}

		return true;

	}

}

?>