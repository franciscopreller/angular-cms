<?php

	// Load the dependency
	require_once 'Controller.class.php';

	/**
	 * Router logic:
	 *
	 * 1.	Get the uri
	 * 1.1.	Separate url from query string params
	 *
	 * 2.	Check if route matches api call
	 * 2.1.	If route matches api call, intercept traffic and perform REST server work
	 * 2.2.	If route does not match api call, load page normally
	 */
	class RESTServer {

		// ==================================================================
		//
		// Properties
		//
		// ------------------------------------------------------------------
		
		private $url;
		private $params;
		private $controller;
		private $pageAlias;
		private $dirName = APP_DIR;

		// ==================================================================
		//
		// Methods
		//
		// ------------------------------------------------------------------
		
		/**
		 * Sets the page alias in case no REST controllers are found
		 * @param string $pageAlias The page alias
		 */
		public function setPageAlias($pageAlias) {
			$this->pageAlias = $pageAlias;

			return $this;
		}

		/**
		 * Gets the page alias
		 * @return string The page alias
		 */
		public function getPageAlias() {
			return $this->pageAlias;
		}

		/**
		 * Gets the URL
		 * @return string The current URL without query string
		 */
		public function getUrl() {
		    return $this->url;
		}
		
		/**
		 * Gets the query string params
		 * @return string The current query string params
		 */
		public function getParams() {
		    return $this->params;
		}

		/**
		 * Gets the controller
		 * @return string The controller
		 */
		public function getController() {
			return $this->controller;
		}

		/**
		 * 1. Gets the URI from the browser
		 * 2. Removes the project directory name from the URL for routing
		 * 3. Finds any additional GET params from the querystring and puts them into an array
		 */
		public function __construct() {
			
			// Get URI
			$uri = $_SERVER['REQUEST_URI'];
			
			// Remove the directory from the URI if the dir name is not the default
			$uri = $this->dirName !== '/' ? str_replace($this->dirName, '', $uri) : $uri;

			// Get the param string
			$uriArray = explode('?', $uri);

			// Set the URL
			$this->url = $uriArray[0];

			// If params string available, store it in a variable
			if (count($uriArray) > 1) {

				// Split param string into key=value strings
				$paramsArray = explode('&', $uriArray[1]);

				// Initialize $params
				$params = array();

				// Iterate over the params array
				foreach($paramsArray as $param) {

					// Store key value pair
					$keyValue = explode('=', $param);

					// Add it to the param array
					$params[$keyValue[0]] = $keyValue[1];

				}

				// Set the params
				$this->params = $params;

			}

		}

		/**
		 * 1. 	Determines which controller we must use.
		 * 1.1. If not controller is found, assigns the default PageController and launches it
		 * 2.	Loads the model if it exists
		 * 3.	Checks if the controller's method exists, if it does, call it
		 * 3.1. If the method does not exist, $firstArgument should switch to 404
		 */
		public function Load() {

			$urlArray      = explode('/', $this->url);

			// Last minute hack :(
			// Check if the firstargument is empty, if it is, remove it and re-order the url array
			if (empty($urlArray[0])) {

				unset($urlArray[0]);
				$urlArray = array_values($urlArray);

			}

			$firstArgument = $urlArray[0];
			$args          = array();

			// Check that there at least 2 parts to the url, and that the first
			// parameter is 'rest'. If match found, load rest server MVC
			if (count($urlArray) > 1 && $firstArgument === 'rest') {

				// Set the controller
				$controller = $urlArray[1];

				// Load the controller
				$this->LoadController($controller);

				// If the query contains a valid method, load it as the view (response)
				if (count($urlArray) > 2) {

					// Assign the view
					$view = ucfirst($urlArray[2]);

					// Args?
					if (count($urlArray) > 3) {

						// Set args
						$args = $urlArray;
						unset($args[0]);
						unset($args[1]);
						unset($args[2]);

						// Re-order args array
						$args = array_values($args);

					}

					// Load view
					$this->LoadView($view, $args);

				}

				// Assign reminding arguments to a args variable
				$args = $urlArray;
				unset($args[0]);
				unset($args[1]);

				// Re-order args array
				$args = array_values($args);

				// Attempt to load the main view
				$this->LoadDefaultView($args);

			}

			// Check if the firstArgument param is 'admin', if so route to the admin controller
			elseif ($firstArgument === 'admin') {
				
				$this->LoadAdmin();

			}

			// If we made it this far, we can set the first argument as thepage alias
			// so it can be retrieved by the page controller to load the web site
			$this->setPageAlias($firstArgument);
		}

		/**
		 * Loads a controller if it exists, also requires it as a dependency
		 * @param string $controller The controller name to load.
		 */
		private function LoadController($controller) {

			// Get the controller name
			$controllerFileName = $controller . '.controller.php';

			// Get the full file name and path for the controller
			$controllerPath     = sprintf('%1$s/../controllers/%2$s', dirname(__FILE__), $controllerFileName);

			// If the controller exists, assign it
			if (file_exists($controllerPath)) {

				$this->controller = ucfirst($controller) . 'Controller';

				// Load the model if it exists
				$this->LoadModel($controller);

				// Load the controller
				require_once $controllerPath;

			}

		}

		/**
		 * Loads the view for the controller, in the case of this REST server,
		 * the view refers to the method to be called within the controller.
		 * @param string $view The name of the view (method).
		 */
		private function LoadView($view, $args) {

			// Check if the method exists
			if (method_exists($this->controller, $view)) {
				
				// Call the method
				$controller = new $this->controller($args);
				$controller->$view();

				// Get the response
				$controller->GetJsonResponse();

			}

		}

		/**
		 * Attempts to load the default view.
		 */
		private function LoadDefaultView($id) {

			// Check if the method exists
			if (method_exists($this->controller, 'Main')) {
				
				// Call the method
				$controller = new $this->controller($id);
				$controller->Main();

				// Get the response
				$controller->GetJsonResponse();

			}

		}

		/**
		 * Loads the model for the controller if it exists
		 * @param string $controller The name of the controller for which a model is being loaded for.
		 */
		private function LoadModel($controller) {

			// Get the model file name
			$modelFileName = $controller . '.class.php';

			// Get the model path
			$modelPath     = sprintf('%1$s/../classes/%2$s', dirname(__FILE__), $modelFileName);

			// Check if the file exists, load it
			if (file_exists($modelPath)) require_once $modelPath;

		}

		/**
		 * Loads the admin view.
		 * Since this loads the admin controls, this is the best place to authenticate.
		 */
		private function LoadAdmin() {

			// If user is logged in, allow them through to the admin area
			if (Auth::LoggedIn()) {

				// Get admin view
				$adminView = sprintf('%1$s/../views/admin.view.php', dirname(__FILE__));

				// Include the admin view
				include $adminView;

			// Otherwise, send them to the login screen
			} else {

				// Get the login view
				$loginView = sprintf('%1$s/../views/login.view.php', dirname(__FILE__));

				// Include the login view
				include $loginView;

			}

			// Stop process here...
			die();

		}

	}

?>