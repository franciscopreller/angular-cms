<?php

    //PHP References
    require_once 'classes/DAL.class.php';

?><!doctype html>
<html>
<head>
    <title>CMS Test</title>
</head>
<body>
    <h1>Test Page for the CMS Project</h1>

    <?php

        /**
         * Test our DAL
         */

        //Instantiate the DAL
        $dal = new DAL();

        //Define a query
        $sql = "SELECT CONCAT(LastName, ', ', FirstName) AS FullName
                FROM user
                WHERE Username = :Username";

        //Define parameters
        $parameters = array(
            array(
                'name'=>':Username',
                'value'=>'admin',
                'type'=>PDO::PARAM_STR
            )
        );

        //Execute query
        $result = $dal->executeScalar($sql, $parameters);

        //Do something with the result
        echo "Value: $result";

    ?>

</body>
</html>