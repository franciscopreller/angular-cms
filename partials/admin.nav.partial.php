<!-- Bootstrap Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <ul class="span10 nav">

                    <!-- Angular to display the menu -->
                    <li ng-repeat="nav in navItems" ng-switch="nav.newTab">
                        <a ng-switch-when="false" ng-href="admin/{{ nav.url }}">{{ nav.title }}</a>
                        <a ng-switch-when="true" ng-href="{{ nav.url }}" target="_blank">{{ nav.title }}</a>
                    </li>
                    
                </ul>
                <ul class="span2 nav">
                    <li class="pull-right">
                        <a ng-href="" ng-click="logout()"><i class="icon icon-white icon-off"></i> Logout</a>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</nav>  <!--/ Navigation -->