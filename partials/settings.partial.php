<form id="formSetting" novalidate ng-submit="updateSettings()">
    <fieldset>
        <legend>Settings</legend>
        <alert-container></alert-container>

        <section class="well">
            <em>
                To add settings, click on the 'Add Setting' button found at the bottom of the page.<br>
                To edit settings, make the desired changes on this page and click 'Save Changes'.<br>
                To delete a setting, hover over the desired setting and click on the remove button.<br>
            </em>
        </section>

        <table class="table table-hover settings-list">

            <thead>
                <tr>
                    <th>Setting Name</th>
                    <th>Setting Value</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
                <tr ng-repeat="setting in settings">
                    <td>{{ setting.SettingName }}</td>
                    <td><input type="text" ng-model="setting.SettingValue"></td>
                    <td class="text-right">
                        <a href="" class="close" ng-click="confirmDelete(setting)" show-on-parent-hover="tr">&times;</a>
                    </td>
                </tr>
            </tbody>

        </table>

        <section class="setting-options form-actions">
            <button class="btn btn-success" type="button" ng-click="displayDialog()"><i class="icon icon-white icon-plus"></i> Add Setting</button>
            <button class="btn btn-primary" type="submit"><i class="icon icon-white icon-ok"></i> Save Changes</button>
        </section>

    </fieldset>
</form>