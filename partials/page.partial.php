<ng-form id="formPage">
    <fieldset class="container">

    	<!-- Title -->
        <legend ng-switch on="type">
        	<span ng-switch-when="edit">
        		<a ng-href="admin/pages">Pages</a> &gt; <strong>{{ page.MenuTitle }}</strong>
        	</span>
        	<span ng-switch-when="create">
        		<a ng-href="admin/pages">Pages</a> &gt; <strong>Create New</strong>
        	</span>
        </legend>

        <!-- Buttons -->
		<div class="row">

			<div class="span10">
				<!-- Alerts -->
        		<alert-container></alert-container>
			</div>

			<div class="span2 clearfix pages-buttons">
				<button type="submit" class="btn btn-primary pull-right" ng-click="submitPage()">Publish</button>
			</div>

		</div>

		<div class="row">

			<div class="span8 edit-fields">

				<div class="well">

					<!-- Main content section -->
					<section>

						<label>Page Title</label>
						<input type="text" ng-model="page.fields[0].FieldValue">

						<label>Page Content</label>
						<textarea ng-model="page.fields[1].FieldValue"></textarea>

					</section>

					<!-- Sidebar section: Only show if first layout is not picked -->
					<section ng-show="page.LayoutID != 1">

						<label>Sidebar Title</label>
						<input type="text" ng-model="page.fields[2].FieldValue">

						<label>Sidebar Content</label>
						<textarea ng-model="page.fields[3].FieldValue"></textarea>

					</section>

				</div>

			</div>

			<div class="span4">

				<div class="well">

					<label for="PageAlias">Page Alias</label>
					<input type="text" name="PageAlias" ng-model="page.PageAlias" required>

					<label for="MenuTitle">Menu Title</label>
					<input type="text" name="MenuTitle" ng-model="page.MenuTitle" required>

					<label for="LayoutID">Layout</label>
					<select name="LayoutID" ng-model="page.LayoutID" ng-options="layout.LayoutID as layout.LayoutName for layout in layouts"></select>

					<label for="ParentID">Parent</label>
					<select name="ParentID" ng-model="page.ParentID" ng-options="parent.ParentID as parent.ParentMenuTitle for parent in parents">
						<option value="">-- None --</option>
					</select>
					
					<label for="MenuOrder">Menu Order</label>
					<input type="text" name="MenuOrder" ng-model="page.MenuOrder" required>

					<label for="ShowInMenu">Show in Menu</label>
					<select name="ShowInMenu" ng-model="page.ShowInMenu" ng-options="show.ShowInMenu as show.Display for show in showOptions"></select>

					<label for="Active">Active</label>
					<select name="Active" ng-model="page.Active" ng-options="active.Active as active.Display for active in activeOptions"></select>

				</div>

			</div>

		</div>

    </fieldset>
</ng-form>