<form id="formPages" novalidate>
    <fieldset>
        <legend>Pages</legend>

            <section class="well">
                <em>
                    To add a new page, click the 'Add Page' button found at the bottom of the page.<br>
                    To edit a page, simply click on the corresponding row.
                </em>
            </section>

            <table class="table table-hover pages-list">

                <thead>
                    <tr>
                        <th>Page ID</th>
                        <th>Alias</th>
                        <th>Menu Title</th>
                        <th>Layout</th>
                        <th>Parent</th>
                        <th>Order</th>
                        <th>Shown in Menu</th>
                        <th>Active</th>
                    </tr>
                </thead>

                <tbody>
                    <tr ng-repeat="page in pages" ng-click="go(page.PageAlias)">
                        <td>{{ page.PageID }}</td>
                        <td>{{ page.PageAlias }}</td>
    					<td>{{ page.MenuTitle }}</td>
    					<td>{{ page.LayoutName }}</td>
    					<td>{{ page.ParentMenuTitle }}</td>
    					<td>{{ page.MenuOrder }}</td>
    					<td>{{ page.ShowInMenu }}</td>
    					<td>{{ page.Active }}</td>
                    </tr>
                </tbody>

            </table>

        <section class="pages-options form-actions">
            <a ng-href="admin/page/new" class="btn btn-success"><i class="icon icon-white icon-plus"></i> Add Page</a>
        </section>

    </fieldset>
</form>