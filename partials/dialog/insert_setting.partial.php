<div class="modal">
	<div class="modal-header">
		<h2>Add your setting</h2>
	</div>
	<form name="addSetting" ng-submit="saveSettings()">
		<div class="modal-body">
			<div class="clearfix" ng-repeat="setting in settings">

				<ng-form name="settingForm" novalidate>

					<!-- Setting name -->
					<section class="span3 pull-left">
						<input
							type="text"
							name="SettingName"
							placeholder="Setting Name"
							ng-model="setting.SettingName"
							ng-unique="Setting.SettingName"
							required validator>

						<!-- Validation error messages -->
						<div class="alert alert-error" ng-show="settingForm.SettingName.$dirty && settingForm.SettingName.$invalid">
							<strong>Error:</strong>
							<div class="text-error" ng-show="settingForm.SettingName.$error.required">
								<strong>&times;</strong> Value is required.
							</div>
							<div class="text-error" ng-show="settingForm.SettingName.$error.unique">
								<strong>&times;</strong> Must be unique.
							</div>
						</div>

					</section>

					<!-- Setting value -->
					<section class="span3 pull-left">
						<input
							type="text" 
							name="SettingValue"
							placeholder="Setting Value" 
							ng-model="setting.SettingValue" 
							required validator>

						<!-- Validation error messages -->
						<div class="alert alert-error" ng-show="settingForm.SettingValue.$dirty && settingForm.SettingValue.$invalid">
							<strong>Error:</strong>
							<div class="text-error" ng-show="settingForm.SettingValue.$error.required">
								<strong>&times;</strong> Value is required.
							</div>
						</div>

					</section>

				</ng-form>

				<!-- Close icon -->
				<a class="pull-right" href="" ng-click="removeRow($index)" ng-show="!$first"><i class="icon icon-remove"></i></a>
			</div>
			<a class="pull-right" ng-click="addRow()">Add another</a>
		</div>
		<div class="modal-footer">
			<button ng-click="closeDialog()" type="button" class="btn">Cancel</button>
			<button type="submit" class="btn btn-primary">
				<i class="icon icon-ok icon-white"></i> Save
			</button>
		</div>
	</form>
</div>