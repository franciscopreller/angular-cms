<div class="modal">
	<div class="modal-header">
	</div>
	<div class="modal-body">
		<p class="text-center">
			You are about to permanently delete the setting: <strong>{{ setting.SettingName }}</strong>
			<br>
			<span class="lead">Continue?</span>
		</p>
	</div>
	<div class="modal-footer">
		<button ng-click="closeDialog()" class="btn">Cancel</button>
		<button ng-click="deleteSetting()" class="btn btn-danger"><i class="icon icon-trash icon-white"></i> Do it!</button>
	</div>
</div>