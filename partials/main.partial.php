<?php include '../config.inc.php'; ?>
<h2>Main</h2>

<p>To manage your site using CMStastic, please use the menu at the top of the page.</p>

<ul>
    <li><a href="admin/pages">Pages</a></li>
    <li><a href="admin/settings">Settings</a></li>
    <li><a href="<?php echo WEBSITE; ?>" target="_blank">View Site</a></li>
</ul>