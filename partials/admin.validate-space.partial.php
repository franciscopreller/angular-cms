<div class="popover bottom popover-validate">
    <div class="arrow"></div>
    <div class="popover-content">
        <div class="text-error" ng-repeat="error in errors"><strong>&times;</strong> {{ error }} </div>
    </div>
</div>