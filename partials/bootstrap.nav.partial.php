<!-- Bootstrap Navigation -->
<nav class="navbar navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="nav-collapse collapse">
                <ul class="nav">

                    <!-- Angular to display the menu -->
                    <li ng-repeat="nav in navItems" ng-class="{'dropdown':nav.dropdown}" ng-switch="nav.dropdown">
                        <a ng-switch-when="false" ng-href="{{ nav.url }}">{{ nav.title }}</a>
                        <a ng-switch-when="true" href="#" class="dropdown-toggle" data-toggle="dropdown">
                            {{ nav.title }} <b class="caret" ng-show="nav.dropdown"></b>
                        </a>
                        <ul class="dropdown-menu" ng-show="nav.dropdown">
                            <li ng-repeat="subnav in nav.children">
                                <a ng-href="{{ subnav.url }}">{{ subnav.title }}</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</nav>  <!--/ Navigation -->