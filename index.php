<?php

	// Set session
	session_start();

	// Error handling
	ini_set('error_reporting',  'On');
	ini_set('display_errors',   'On');
	error_reporting(E_ALL);

	// Include config
	include 'config.inc.php';

	// Load dependencies
	require_once 'classes/DAL.class.php';
	require_once 'classes/RESTServer.class.php';
	require_once 'classes/Page.class.php';
	require_once 'classes/Pages.class.php';
	require_once 'classes/Setting.class.php';
	require_once 'classes/Auth.class.php';
	require_once 'classes/Menu.class.php';
	
	// Check the URL and match any routes available
	$rest = new RESTServer();

	// If an appropriate controller exists, load it, otherwise do nothing
	$rest->Load();

	// Load the page controller
	require_once 'controllers/page.controller.php';

	// Get the page alias from the rest server routing controls
	$pageAlias = $rest->getPageAlias();

	// Start the new object
	new PageController($pageAlias);

?>