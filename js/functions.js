/**
 * Forces navigation back home
 */
var navigateHome = function() {
	'use strict';
	window.location = window.location.origin + window.location.pathname;
};