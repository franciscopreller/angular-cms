/*global angular */
"use strict";

angular.module('cmsApp',
	['ui.bootstrap', 'cmsApp.filters', 'cmsApp.services', 'cmsApp.directives', 'cmsApp.controllers']);

angular.module('adminApp',
	['ui.bootstrap', 'adminApp.filters', 'adminApp.services', 'adminApp.directives', 'adminApp.controllers'])

	.config(['$routeProvider', '$locationProvider', '$tooltipProvider', function($routeProvider, $locationProvider, $tooltipProvider) {

		/**
		 * Sets the app routes.
		 * App routes will intercept the click events and prevent the page from actually
		 * navigating away or reloading. The routeProvider will then change the actual
		 * value of the URL in the address bar and display the ngView according to the
		 * route. Linked via the templateUrl
		 */
		$routeProvider
			.when('/admin/settings', { templateUrl: 'partials/settings.partial.php', controller: 'SettingsCtrl' })
			.when('/admin/pages', { templateUrl: 'partials/pages.partial.php', controller: 'PagesCtrl' })
			.when('/admin/page/new', { templateUrl: 'partials/page.partial.php', controller: 'NewPageCtrl' })
			.when('/admin/page/:pageAlias', { templateUrl: 'partials/page.partial.php', controller: 'PageCtrl' })
			.when('/admin/', { templateUrl: 'partials/main.partial.php' })
			.otherwise({redirectTo: '/admin/' });

		/**
		 * HTML5 mode active aids with having to use the hasbang operator in addresses
		 * #/admin/settings = ugly :(
		 */
		$locationProvider
		  	.html5Mode(true);

		/**
		 * The tooltip provider ties in to ui.bootstrap (an AngularJS directive which ports twitter
		 * bootstrap without the need for jQuery). This just adds two custom events which are created
		 * in related directives to perform validation checks.
		 */
		$tooltipProvider
			.setTriggers({ 'inputInvalid' : 'inputValid' });

	}]);