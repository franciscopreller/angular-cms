/*global angular */
"use strict";

angular.module('cmsApp.directives', [])

	/**
	 * A template reference to a <bootstrap-nav> element
	 */
	.directive('bootstrapNav', function() {

		return {
			restrict	: 'E',
			templateUrl : 'partials/bootstrap.nav.partial.php',
			controller	: 'MenuCtrl'
		};

	});

angular.module('adminApp.directives', [])

	/**
	 * A template reference to an <admin-nav> element
	 */
	.directive('adminNav', function() {

		return {
			restrict	: 'E',
			templateUrl : 'partials/admin.nav.partial.php',
			controller	: 'AdminMenuCtrl'
		};

	})

	/**
	 * A template reference to an <alert-container> element
	 */
	.directive('alertContainer', function() {

		return {
			restrict	: 'E',
			templateUrl	: 'partials/admin.alerts.partial.php',
			controller	: 'AlertCtrl'
		};

	})

	.directive('validator', [function() {

		return {
			restrict    : 'A',
			require     : 'ngModel',
			link        : function (scope, element, attrs, ctrl) {

				// The element
				element

					// Bind to blur, disallow
					.bind('blur', function() {
						// If element is invalid, send a custom event to the element
						if (element.hasClass('ng-invalid')) {

							// If the user is moving away from a required field, 
							// remove some angular form flags from the form
							ctrl.$dirty = true;

							// Force focus on invalid element
							element[0].focus();
						}
					});

			}
		};

	}])

	/**
	 * Validates for unique field in table
	 * @param  {object} async AngularJS $http service
	 */
	.directive('ngUnique', ['$http', function($http) {

		return {
			require : 'ngModel',
			link    : function (scope, element, attrs, ctrl) {

				// Hook onto the blur event on the attribute
				element.bind('blur', function (event) {

					// Reference: http://blog.brunoscopelliti.com/form-validation-the-angularjs-way
					scope.$apply(function() {

						// Contact the server
						$http.post('rest/validation/FieldIsUnique', {
							value   : element.val(),
							dbField : attrs.ngUnique
						})

						// Action response
						.success(function(data) {

							var result = JSON.parse(data);

							// Send to controller
							ctrl.$setValidity('unique', result);

							// Focus if false
							if (!result)
								element[0].focus();
							
						});


					});

				});

			}
		}

	}])

	/**
	 * A directive for an attribute which can be used inside other tags, this will hide them from view
	 * until the parent element is hovered over. Optionally, pass a parameter for a element type
	 * and the directive will iterate until it finds a match for the tag and bind to it.
	 *
	 * Example usage: <div show-on-parent-hover="section"></div>
	 * 
	 * This will hide the div until the next up section element is hovered over.
	 */
	.directive('showOnParentHover', function() {

		return {
			restrict : 'A',
			link     : function(scope, element, attrs) {

				// Bind parentElement to itself first, as a fallback
				var parentElement = element;

				// Get value passed by directive if any
				if (element[0].attributes['show-on-parent-hover'].value.length) {
					
					// Convert element type to upper case so we are able to match to nodeName
					var elementType = (element[0].attributes['show-on-parent-hover'].value).toUpperCase();

					// jQLite does not provide the jQuery method parents() which would have been ideal for this
					// situation, however since the aim is to be independent from jQuery, we will iterate until
					// we find the nodeName for the element type we are looking for
					do {

						// Otherwise assign this node to its parent and continue
						parentElement = parentElement.parent();

						// As a fallback, we will kill if we get to the body element
						if (parentElement[0].nodeName === "BODY") {

							// Assign it back to the initial element's parent before breaking
							parentElement = element.parent();
							break;
						}

					} while (parentElement[0].nodeName !== elementType);

				// If the directive does not have a value, just default to its parent element by default
				} else
					parentElement = element.parent();

				// Bind to mousenter using jQLite
				parentElement.bind('mouseenter', function() { element[0].style.display = 'block'; });

				// Bind to mouseleave using jQLite
				parentElement.bind('mouseleave', function() { element[0].style.display = 'none'; });

			}
		}

	});