/*global angular */
'use strict';

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('cmsApp.services', []);

angular.module('adminApp.services', ['ngResource'])

	.factory('Validation', [function() {

		var error = null;

		return {
			error: function(error) { error = error; },
			clear: function() { error = null; },
			msg  : function() { return error; }
		};
	}])

	/**
	 * Handles visible alerts to client by adding and deleting them from the alerts queue
	 */
	.factory('Alerts', function() {

		var alerts = [];

		return {
			add  : function(type, msg) { alerts.push({ type: type, msg: msg }); },
			close: function(index) { alerts.splice(index, 1); },
			get  : function(controller) { return alerts; },
			clear: function() { alerts = []; }
		};
	})

	/**
	 * REST interface for site settings
	 * @param  {object} $http     AngularJS HTTP directive
	 * @param  {object} $resource AngularJS angular-resource library
	 * @return {object}           Resource object
	 */
	.factory('Settings', ['$http', '$resource', function($http, $resource) {

		return $resource('rest/setting/:settingName', {settingName: '@settingName'}, {
			save   : { 
				method  : 'POST',
				isArray : true,
				transformResponse: [function (data, headersGetter) {
					return [{ result: JSON.parse(data) }];
				}].concat($http.defaults.transformResponse)
			},
			update : {
				method: 'PUT',
				isArray: true,
				transformResponse: [function (data, headersGetter) {
					return [{ result: JSON.parse(data) }];
				}].concat($http.defaults.transformResponse)
			},
			delete : {
				method: 'DELETE',
				params: { settingName: '@settingName' },
				transformResponse: [function (data, headersGetter) {
					return { result: JSON.parse(data) };
				}].concat($http.defaults.transformResponse)
			}
		});

	}])

	/**
	 * REST interface for site pages
	 * @param  {object} $http     AngularJS HTTP directive
	 * @param  {object} $resource AngularJS angular-resource library
	 * @return {object}           Resource object
	 */
	.factory('Pages', ['$http', '$resource', function($http, $resource) {

		return $resource('rest/pages/:pageAlias', {pageAlias: '@pageAlias'}, {
			get : {
				method: 'GET',
				isArray: true,
				transformResponse: [function (data, headersGetter) {
					return [{ result: JSON.parse(data) }];
				}].concat($http.defaults.transformResponse)
			},
			save: {
				method  : 'POST',
				isArray : true,
				transformResponse: [function (data, headersGetter) {
					return [{ result: JSON.parse(data) }];
				}].concat($http.defaults.transformResponse)
			},
			update : {
				method: 'PUT',
				isArray: true,
				transformResponse: [function (data, headersGetter) {
					return [{ result: JSON.parse(data) }];
				}].concat($http.defaults.transformResponse)
			}
		});

	}]);