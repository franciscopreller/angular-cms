/*global angular */
"use strict";

angular.module('cmsApp.controllers', [])

	/**
	 * Menu controller
	 * @param {object} $scope AngularJS scope
	 * @param {object} $http  AngularJS $http directive
	 */
	.controller('MenuCtrl', ['$scope', '$http', function($scope, $http) {

		/**
		 * Gets the menu object from the server and assigns it to the scope
		 */
		$http.get('rest/menu/GetMenuObject')

			.success(function(data) {

				// Assign navigation items
				$scope.navItems = data;

			});

	}]);

angular.module('adminApp.controllers', [])

	/**
	 * Admin Menu controller
	 * @param {object} $scope AngularJS scope
	 * @param {object} $http  AngularJS $http directive
	 */
	.controller('AdminMenuCtrl', ['$scope', '$http', '$window', function($scope, $http, $window) {

		/**
		 * Requests the admin menu from the server and assigns it to the scope
		 */
		$http.get('rest/menu/GetAdminMenuObject')

			.success(function(data) {

				// Assign navigation items
				$scope.navItems = data;

			});

		/**
		 * Log out user
		 */
		$scope.logout = function() {

			$http.delete('rest/login').success(function(data) {

				if (data) $window.location.reload();

			});

		};

	}])

	/**
	 * Login controller
	 * @param  {object} $scope  AngularJS scope
	 * @param  {object} $http   AngularJS http service
	 * @param  {object} $window AngularJS window service
	 * @param  {object} Alerts  AngularJS Alerts service
	 */
	.controller('LoginCtrl', ['$scope', '$http', '$window', 'Alerts', function($scope, $http, $window, Alerts) {

		$scope.loginDetails = {};

		$scope.submitLogin = function() {

			$http.post('rest/login', $scope.loginDetails).success(function(data) {

				var result = JSON.parse(data);

				// If data returns as true, login successful, full refresh will result in admin panel view
				if (result)
					$window.location.reload();

				else
					Alerts.add('error', 'Unable to login! Either the username or the password was incorrect!');

			});

		};

	}])

	/**
	 * Alerts controller displays any alerts from the software
	 * @param  {object} $scope     AngularJS scope
	 * @param  {object} $rootScope AngularJS rootscope
	 * @param  {object} Alerts     AngularJS Alerts service
	 */
	.controller('AlertCtrl', ['$scope', '$rootScope', 'Alerts', function($scope, $rootScope, Alerts) {

		$scope.alerts = Alerts.get();

		// Clear alerts on page change
		$rootScope.$on('$routeChangeStart', function() {
			Alerts.clear();
		});

		// Add alert to queue
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};

	}])

	/**
	 * Settings controller
	 * @param {object} $scope  AngularJS scope
	 * @param {object} $http   AngularJS $http directive
	 * @param {object} $dialog AngularJS bootstrap-ui dialog directive
	 * @param {object} Alerts  AngularJS Alerts service
	 */
	.controller('SettingsCtrl', ['$scope', 'Settings', '$dialog', 'Alerts', function($scope, Settings, $dialog, Alerts) {

		$scope.settings = Settings.query();

		/**
		 * Displays the insert dialog and actions the result when it closes
		 */
		$scope.displayDialog = function() {

			// Start dialog with required settings
			var dialog = $dialog.dialog({
				backdrop		: true,
				keyboard		: true,
				backdropClick	: true,
				templateUrl		: 'partials/dialog/insert_setting.partial.php',
				controller		: 'InsertSettingCtrl'
			});

			// Open the dialog and wait for a result when it closes
			dialog.open().then(function(result, settings) {

				// If result returns a value, refresh the settings
				if (result)
					$scope.settings = Settings.query();

		    });

		};

		/**
		 * Displays setting deletion dialog prompt and actions the result when it closes
		 * @param  {string} setting The setting to delete
		 */
		$scope.confirmDelete = function(setting) {

			// Set item to edit
			var itemToEdit = setting;

			// Start dialog with required settings
			$dialog.dialog({
				backdrop		: true,
				keyboard		: true,
				backdropClick	: true,
				templateUrl		: 'partials/dialog/delete_setting.partial.php',
				controller		: 'DeleteSettingCtrl',
				resolve			: { setting: function() { return angular.copy(itemToEdit) } }
			})

			.open().then(function(result, settings) {

				// If result returns a value, refresh the settings
				if (result)
					$scope.settings = Settings.query();

		    });

		};

		/**
		 * Updates all the settings from the settings page
		 */
		$scope.updateSettings = function() {

			Settings.update({}, $scope.settings, function(data) {

				// If added send the success message to the alerts queue
				if (data[0].result)
					Alerts.add('success', 'Excellent. Your settings were successfully updated.');
				
				// Otherwise send an error message to the alerts queue
				else
					Alerts.add('error', 'Ut oh. There was an error while trying to update your settings...');

			});

		};

	}])

	/**
	 * Controller for the insert settings dialog
	 * @param  {object} $scope   AngularJS scope
	 * @param  {object} dialog   AngularJS bootstrap-ui dialog directive
	 * @param  {object} Settings AngularJS Settings service
	 * @param  {object} Alerts   AngularJS Alerts service
	 */
	.controller('InsertSettingCtrl', ['$scope', 'dialog', 'Settings', 'Alerts', function($scope, dialog, Settings, Alerts) {

		$scope.settings = [{ SettingName: null, SettingValue: null }];

		/**
		 * Adds a new row to the total rows displayed for inserting
		 */
		$scope.addRow = function() {
			$scope.settings.push({ SettingName: null, SettingValue: null });
		};

		/**
		 * Removes a row by index
		 * @param  {integer} index The row index number
		 */
		$scope.removeRow = function(index) {
			$scope.settings.splice(index, 1);
		};

		/**
		 * Close the dialog with no result sent (cancel)
		 */
		$scope.closeDialog = function() {
			dialog.close();
		};

		/**
		 * Contact the server using our Settings service and add a new setting,
		 * then send confirmation to dialog and close it
		 */
		$scope.saveSettings = function() {

			Settings.save({}, $scope.settings, function(data) {

				// If added send the success message to the alerts queue
				if (data[0].result)
					Alerts.add('success', 'Great! You have successfully added new settings!');
				
				// Otherwise send an error message to the alerts queue
				else
					Alerts.add('error', 'There seems to have been an error and we could not add the new settings.');

				dialog.close(data[0].result);

			});

		};

	}])

	/**
	 * Controller for the delete setting prompt
	 * @param  {object} $scope   AngularJS scope
	 * @param  {object} setting  The setting being deleted
	 * @param  {object} dialog   The dialog
	 * @param  {object} Settings AngularJS Settings service
	 * @param  {object} Alerts   AngularJS Alerts service
	 */
	.controller('DeleteSettingCtrl', ['$scope', 'setting', 'dialog', 'Settings', 'Alerts', function($scope, setting, dialog, Settings, Alerts) {

		$scope.setting = setting;

		/**
		 * Close the dialog with no result sent (cancel)
		 */
		$scope.closeDialog = function() {
			dialog.close();
		};

		/**
		 * Contact the server using the Settings service and delete an existing setting.
		 * Send confirmation back to the client on wether this succeeded or failed.
		 */
		$scope.deleteSetting = function() {

			Settings.delete({ settingName: $scope.setting.SettingName }, {}, function(data) {

				// If added send the success message to the alerts queue
				if (data.result)
					Alerts.add('success', 'The setting "' + $scope.setting.SettingName + '" was permanently deleted.');
				
				// Otherwise send an error message to the alerts queue
				else
					Alerts.add('error', 'Could not delete the setting "' + $scope.setting.SettingName + '"');

				dialog.close(data.result);

			})

		};

	}])

	/**
	 * Pages Controller
	 * @param  {object} $scope   AngularJS scope
	 * @param  {object} Pages    AngularJS pages service
	 * @param  {object} Alerts   AngularJS alerts service
	 */
	.controller('PagesCtrl', ['$scope', '$location', 'Pages', 'Alerts', function($scope, $location, Pages, Alerts) {

		$scope.pages = Pages.query();

		$scope.go = function(page) {
			$location.path('/admin/page/' + page);
		};

	}])

	/**
	 * Single page Controller
	 * @param  {object} $scope		 AngularJS scope
	 * @param  {object} $routeParams AngularJS routeParams service
	 * @param  {object} Pages    	 AngularJS pages service
	 * @param  {object} Alerts   	 AngularJS alerts service
	 */
	.controller('PageCtrl', ['$scope', '$http', '$routeParams', 'Pages', 'Alerts', function($scope, $http, $routeParams, Pages, Alerts) {

		// Get the current page info from the URL
		Pages.get({ pageAlias: $routeParams.pageAlias }, {}, function(data) {

			// Assign the ID
			var id = data[0].result.PageID;

			$scope.type = 'edit';

			// Set the page on the scope 
			$scope.page = data[0].result;

			// Get layouts
			$http.get('rest/pages/GetLayouts').success(function(data) { $scope.layouts = data; });

			// Get possible parents
			$http.get('rest/pages/GetParents').success(function(data) { $scope.parents = data; });

			// Show in menu
			$scope.showOptions = [
				{ Display: 'Hidden', ShowInMenu: 0 },
				{ Display: 'Shown',  ShowInMenu: 1 }
			];

			// Active/Inactive selector
			$scope.activeOptions = [
				{ Display: 'Inactive', Active: 0 },
				{ Display: 'Active',   Active: 1 }
			];

			// Clever hacks to select above field's options manually
			$scope.page.ShowInMenu = $scope.showOptions[data[0].result.ShowInMenu].ShowInMenu;
			$scope.page.Active     = $scope.activeOptions[data[0].result.Active].Active;

			// Get all fields by page ID
			$http.get('rest/pages/GetFields/' + id).success(function(data) {

				// Create a fields object to use as a container
				$scope.page.fields = [
					{ FieldID: 1, FieldValue: null },
					{ FieldID: 2, FieldValue: null },
					{ FieldID: 3, FieldValue: null },
					{ FieldID: 4, FieldValue: null }
				];

				// Iterate over data and assign to the right field type
				for (var i = 0; i < data.length; i++)
					$scope.page.fields[i].FieldValue = data[i].FieldValue;

			});
		});

		/**
		 * Edits existing page
		 */
		$scope.submitPage = function() {

			Pages.update({}, $scope.page, function(data) {

				// If added send the success message to the alerts queue
				if (data[0].result)
					Alerts.add('success', 'Great! You have successfully updated the page!');
				
				// Otherwise send an error message to the alerts queue
				else
					Alerts.add('error', 'There seems to have been an error and we could not update the page.');

			});

		};

	}])

	/**
	 * Single page Controller
	 * @param  {object} $scope   	 AngularJS scope
	 * @param  {object} $routeParams AngularJS routeParams service
	 * @param  {object} Pages    	 AngularJS pages service
	 * @param  {object} Alerts   	 AngularJS alerts service
	 */
	.controller('NewPageCtrl', ['$scope', '$http', '$routeParams', 'Pages', 'Alerts', '$location', function($scope, $http, $routeParams, Pages, Alerts, $location) {

		$scope.type = 'create';

		// Get layouts
		$http.get('rest/pages/GetLayouts').success(function(data) {

			$scope.layouts = data;

			// Clever hacks to select above field's options manually
			$scope.page = {
				ShowInMenu : $scope.showOptions[1].ShowInMenu,
				Active     : $scope.activeOptions[1].Active,
				LayoutID   : $scope.layouts[0].LayoutID
			};

			// Create a fields object to use as a container
			$scope.page.fields = [
				{ FieldID: 1, FieldValue: null },
				{ FieldID: 2, FieldValue: null },
				{ FieldID: 3, FieldValue: null },
				{ FieldID: 4, FieldValue: null }
			];

		});

		// Get possible parents
		$http.get('rest/pages/GetParents').success(function(data) { $scope.parents = data; });

		// Show in menu
		$scope.showOptions = [
			{ Display: 'Hidden', ShowInMenu: 0 },
			{ Display: 'Shown',  ShowInMenu: 1 }
		];

		// Active/Inactive selector
		$scope.activeOptions = [
			{ Display: 'Inactive', Active: 0 },
			{ Display: 'Active',   Active: 1 }
		];

		/**
		 * Create new page on submission
		 */
		$scope.submitPage = function() {

			Pages.save({}, $scope.page, function(data) {

				// If added send the success message to the alerts queue
				if (data[0].result) {

					$location.path('admin/page/' + $scope.page.PageAlias);
					setTimeout(function() {
						Alerts.add('success', 'Great! You have successfully added a new page!');
					}, 100);

				}
				
				// Otherwise send an error message to the alerts queue
				else
					Alerts.add('error', 'There seems to have been an error and we could not update the page.');

			});

		};

	}]);