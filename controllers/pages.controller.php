<?php

class PagesController extends Controller {

	public function Main() {

		/**
		 * HTTP Method: GET
		 * Routing: /rest/pages/(:pageAlias)
		 * Get all settings or a single setting
		 */
		if ($this->IsGet()) {

			// Check if the controller has arguments and return the first argument
			if ($this->HasArgs()) {
				// Set response, type cast it to an array
				$this->setResponse(Pages::GetPage($this->getArgs(0)));
			}

			// Otherwise return all the settings
			else
				$this->setResponse(Pages::GetPages());
			
		/**
		 * HTTP Method: POST
		 * Routing: /rest/pages
		 * Create a new setting
		 */
		} elseif ($this->IsPost()) {

			$params = $this->getParams();

			// Attempt to save page and get last ID
			$pageID = Pages::AddPage($params);

			// Save new page
			if (!$pageID) {

				$this->setResponse(false);

			} else {

				// If page updated successfully, proceed and add or update fields as necessary
				$this->setResponse(Pages::AddPageFields($params->fields, $pageID));

			}

		/**
		 * HTTP Method: PUT
		 * Routing: /rest/pages/:pageAlias
		 * Update page
		 */
		} elseif ($this->IsPut()) {

			$params = $this->getParams();

			// Update the page
			if (!Pages::UpdatePage($params)) {

				$this->setResponse(false);

			} else {

				// If page updated successfully, proceed and add or update fields as necessary
				$this->setResponse(Pages::AddPageFields($params->fields, $params->PageID));

			}

		}
		
	}

	/**
	 * Gets all page layouts
	 */
	public function GetLayouts() {
		$this->setResponse(Pages::GetLayouts());
	}

	/**
	 * Gets all the origin pages without children
	 */
	public function GetParents() {
		$this->setResponse(Pages::GetParents());
	}

	/**
	 * Gets all page fields for the passed page, via the first argument
	 */
	public function GetFields() {
		$this->setResponse(Pages::GetPageFields($this->getArgs(0)));
	}

}

?>