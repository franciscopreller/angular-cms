<?php

class SettingController extends Controller {

	public function Main() {

		/**
		 * HTTP Method: GET
		 * Routing: /rest/setting/(:settingName)
		 * Get all settings or a single setting
		 */
		if ($this->IsGet()) {

			// Check if the controller has arguments and return the first argument
			if ($this->HasArgs())
				$this->setResponse(Setting::GetSettingValue($this->getArgs(0)));

			// Otherwise return all the settings
			else
				$this->setResponse(Setting::GetSettings());
			
		/**
		 * HTTP Method: POST
		 * Routing: /rest/setting/:settingName
		 * Create a new setting
		 */
		} elseif ($this->IsPost()) {

			// Iterate over all parameters and add each setting
			foreach ($this->getParams() as $data) {

				// Add each setting, but if any return false, kill the loop and return false to the client
				if (!Setting::AddSetting($data->SettingName, $data->SettingValue)) {
					$this->setResponse(false);
					return;
				}
			}

			// Set response to true
			$this->setResponse(true);

		/**
		 * HTTP Method: DELETE
		 * Routing: /rest/setting/:settingName
		 * Delete an existing setting
		 */
		} elseif ($this->IsDelete()) {

			$this->setResponse(Setting::DeleteSetting($this->getArgs(0)));

		/**
		 * HTTP Method: PUT
		 * Routing: /rest/setting
		 * Update all settings
		 */
		} elseif ($this->IsPut()) {

			// Iterate over all parameters and add update each setting
			foreach ($this->getParams() as $data) {

				if (!Setting::UpdateSetting($data->SettingName, $data->SettingValue)) {
					$this->setResponse(false);
					break;
				}

			}

			// Set the response to true
			$this->setResponse(true);

		}
		
	}

}

?>