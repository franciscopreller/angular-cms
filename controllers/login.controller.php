<?php

class LoginController extends Controller {

	public function Main() {

		/**
		 * We will only listen for a POST type call to login
		 */
		if ($this->IsPost()) {

			$params = $this->getParams();

			// Authenticate user
			if (Auth::AuthenticateUser($params->username, $params->password)) {

				$this->setResponse(Auth::LoginUserAndSetID($params->username, $params->password));

			} else {

				$this->setResponse(false);

			}

		/**
		 * We will only listen for a DELETE type call to logout
		 */
		} elseif ($this->IsDelete()) {

			$this->setResponse(Auth::LogoutUserAndEndSession());

		}

	}

}

?>