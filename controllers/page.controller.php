<?php

class PageController {

	/**
	 * Page controller logic:
	 *
	 * 1.	Check if page has been supplied in the query string (e.g. page=about-us)
	 * 1.1.	If no page specified, use the default page
	 * 1.2. If page specified, use the specified page
	 *
	 * 2.	Check if the page exists
	 * 2.1. If page does not exist, serve a 404 error page
	 * 2.2. If page does exist, continue on...
	 *
	 * 3.	Load page information (create Page object which has access to all the page's information)
	 *
	 * 4.	Determine what layout is used for the page and load that layout
	 *
	 * 5.	Control is now passed over to a layout to include all other information about the page (e.g. page
	 *      title/content, sidebar title/content, etc)
	 */
	public function __construct($pageAlias = '') {

		// Check if the page has been specified
		if (empty($pageAlias)) $pageAlias = Page::GetDefaultPageAlias();

		// Check if the page does not exist
		if (!Page::CheckPageExistsByAlias($pageAlias)) {

			// Get the 404 error page's alias
			$pageAlias = Page::Get404ErrorPageAlias();

		}

		// Create a page object
		$page = new Page($pageAlias);

		// Include the view template
		include $page->GetPageViewTemplate();

	}

}

?>