<?php

class ValidationController extends Controller {

	/**
	 * HTTP METHOD: POST
	 * Checks to see if a field on a table is unique.
	 */
	public function FieldIsUnique() {

		// Get params
		$params = $this->getParams();

		// Check uniqueness of field
		$this->setResponse(Validation::IsUnique($params->dbField, $params->value));

	}

}

?>