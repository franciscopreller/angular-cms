<?php

class MenuController extends Controller {

	/**
	 * Requests the menu object from the model and sets it as
	 * the controller response
	 */
	public function GetMenuObject() {
		$this->setResponse(Menu::GetNavigation());
	}

	/**
	 * Requests the admin menu object from the model and sets it as
	 * the controller response
	 */
	public function GetAdminMenuObject() {
		$this->setResponse(Menu::GetAdminNavigation());
	}

}

?>