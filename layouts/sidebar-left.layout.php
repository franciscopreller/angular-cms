<div class="container">
	<div class="row">
		<div class="span3">

			<h2><?php echo $page->GetFieldValue('Sidebar Title'); ?></h2>

			<?php echo $page->GetFieldValue('Sidebar Content'); ?>

		</div>
		<div class="span9">

			<h2><?php echo $page->GetFieldValue('Page Title'); ?></h2>

			<?php echo $page->GetFieldValue('Page Content'); ?>

		</div>
	</div>
</div>