-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2013 at 09:01 PM
-- Server version: 5.5.27
-- PHP Version: 5.3.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webdev_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `Field`
--

CREATE TABLE IF NOT EXISTS `Field` (
  `FieldID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FieldName` varchar(200) NOT NULL,
  `FieldType` varchar(50) NOT NULL,
  PRIMARY KEY (`FieldID`),
  KEY `FieldType` (`FieldType`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `Field`
--

INSERT INTO `Field` (`FieldID`, `FieldName`, `FieldType`) VALUES
(1, 'Page Title', 'PlainText'),
(2, 'Page Content', 'HTMLText'),
(3, 'Sidebar Title', 'PlainText'),
(4, 'Sidebar Content', 'HTMLText');

-- --------------------------------------------------------

--
-- Table structure for table `FieldType`
--

CREATE TABLE IF NOT EXISTS `FieldType` (
  `FieldType` varchar(50) NOT NULL,
  PRIMARY KEY (`FieldType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `FieldType`
--

INSERT INTO `FieldType` (`FieldType`) VALUES
('HTMLText'),
('PlainText');

-- --------------------------------------------------------

--
-- Table structure for table `Layout`
--

CREATE TABLE IF NOT EXISTS `Layout` (
  `LayoutID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LayoutName` varchar(200) NOT NULL,
  `LayoutFileName` varchar(200) NOT NULL,
  PRIMARY KEY (`LayoutID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Layout`
--

INSERT INTO `Layout` (`LayoutID`, `LayoutName`, `LayoutFileName`) VALUES
(1, 'Full Width Content', 'full-width-content.layout.php'),
(2, 'Sidebar Left', 'sidebar-left.layout.php'),
(3, 'Sidebar Right', 'sidebar-right.layout.php');

-- --------------------------------------------------------

--
-- Table structure for table `Layout_Field`
--

CREATE TABLE IF NOT EXISTS `Layout_Field` (
  `FieldID` int(10) unsigned NOT NULL,
  `LayoutID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`FieldID`,`LayoutID`),
  KEY `LayoutID` (`LayoutID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Layout_Field`
--

INSERT INTO `Layout_Field` (`FieldID`, `LayoutID`) VALUES
(1, 1),
(2, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(1, 3),
(2, 3),
(3, 3),
(4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Page`
--

CREATE TABLE IF NOT EXISTS `Page` (
  `PageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PageAlias` varchar(200) NOT NULL,
  `MenuTitle` varchar(200) NOT NULL,
  `LayoutID` int(10) unsigned NOT NULL,
  `ParentID` int(10) unsigned DEFAULT NULL,
  `MenuOrder` smallint(5) unsigned NOT NULL,
  `ShowInMenu` tinyint(1) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`PageID`),
  UNIQUE KEY `PageAlias` (`PageAlias`),
  KEY `LayoutID` (`LayoutID`),
  KEY `ParentID` (`ParentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Page`
--

INSERT INTO `Page` (`PageID`, `PageAlias`, `MenuTitle`, `LayoutID`, `ParentID`, `MenuOrder`, `ShowInMenu`, `Active`) VALUES
(1, 'home', 'Home', 1, NULL, 1, 1, 1),
(2, 'about-us', 'About', 2, NULL, 2, 1, 1),
(3, 'contact-us', 'Contact', 3, NULL, 3, 1, 1),
(4, 'history', 'History', 1, 2, 1, 1, 1),
(5, 'privacy-policy', 'Privacy Policy', 1, 2, 99, 0, 1),
(6, '404', '404', 1, NULL, 99, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Page_Field`
--

CREATE TABLE IF NOT EXISTS `Page_Field` (
  `PageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FieldID` int(10) unsigned NOT NULL,
  `FieldValue` text,
  PRIMARY KEY (`PageID`,`FieldID`),
  KEY `FieldID` (`FieldID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Page_Field`
--

INSERT INTO `Page_Field` (`PageID`, `FieldID`, `FieldValue`) VALUES
(1, 1, 'Welcome to our site!'),
(1, 2, '<p>Welcome...</p>\r\n<p>Here, have some content.</p>'),
(2, 1, 'About Our Company'),
(2, 2, '<p>Info about our company</p>\r\n<ul>\r\n  <li>one</lu>\r\n  <li>two</li>\r\n  <li>three</li>\r\n</ul>'),
(2, 3, 'Did you know?'),
(2, 4, '<ul>\r\n  <li>Fact 1</li>\r\n  <li>Fact 2</li>\r\n  <li>Fact 3</li>\r\n</ul>'),
(3, 1, 'Contact Information'),
(3, 2, '<p>Contact info goes here...</p>'),
(3, 3, 'Opening Hours'),
(3, 4, '<ul>\r\n  <li>Mon-Fri</li>\r\n  <li>Sat</li>\r\n  <li>Sun</li>\r\n</ul>'),
(4, 1, 'History!'),
(4, 2, '<p>Have some history.</p>'),
(6, 1, '404 Page Not Found'),
(6, 2, '<p><strong>Oops, I seem to have lost your page!</strong></p>\n<p>Maybe you should just go <a href="/" onclick="navigateHome(); return false;">home</a>.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `Role`
--

CREATE TABLE IF NOT EXISTS `Role` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(200) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Role`
--

INSERT INTO `Role` (`RoleID`, `RoleName`) VALUES
(1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `Setting`
--

CREATE TABLE IF NOT EXISTS `Setting` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingName` varchar(200) NOT NULL,
  `SettingValue` text,
  PRIMARY KEY (`SettingID`),
  UNIQUE KEY `SettingName` (`SettingName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Setting`
--

INSERT INTO `Setting` (`SettingID`, `SettingName`, `SettingValue`) VALUES
(1, 'Site Title', 'ABC Company'),
(2, 'Site Tagline', 'What a great company!'),
(3, 'Copyright Text', '&copy; 2013 ABC Company. All Rights reserved.');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Password` char(40) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username` (`Username`),
  KEY `RoleID` (`RoleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`UserID`, `Username`, `FirstName`, `LastName`, `Email`, `Password`, `RoleID`) VALUES
(1, 'admin', 'Francisco', 'Preller', 'francisco.preller@gmail.com', 'password', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Field`
--
ALTER TABLE `Field`
  ADD CONSTRAINT `field_ibfk_1` FOREIGN KEY (`FieldType`) REFERENCES `FieldType` (`FieldType`);

--
-- Constraints for table `Layout_Field`
--
ALTER TABLE `Layout_Field`
  ADD CONSTRAINT `layout_field_ibfk_1` FOREIGN KEY (`FieldID`) REFERENCES `Field` (`FieldID`) ON DELETE CASCADE,
  ADD CONSTRAINT `layout_field_ibfk_2` FOREIGN KEY (`LayoutID`) REFERENCES `Layout` (`LayoutID`) ON DELETE CASCADE;

--
-- Constraints for table `Page`
--
ALTER TABLE `Page`
  ADD CONSTRAINT `page_ibfk_1` FOREIGN KEY (`LayoutID`) REFERENCES `Layout` (`LayoutID`),
  ADD CONSTRAINT `page_ibfk_2` FOREIGN KEY (`ParentID`) REFERENCES `Page` (`PageID`);

--
-- Constraints for table `Page_Field`
--
ALTER TABLE `Page_Field`
  ADD CONSTRAINT `page_field_ibfk_1` FOREIGN KEY (`PageID`) REFERENCES `Page` (`PageID`) ON DELETE CASCADE,
  ADD CONSTRAINT `page_field_ibfk_2` FOREIGN KEY (`FieldID`) REFERENCES `Field` (`FieldID`) ON DELETE CASCADE;

--
-- Constraints for table `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`RoleID`) REFERENCES `Role` (`RoleID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
