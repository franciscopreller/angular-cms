<!DOCTYPE html>
<html ng-app="adminApp">
	<head>
		<title>CMS Test</title>
		<link rel="stylesheet" type="text/css" href="<?php Page::HomePage(); ?>css/style.css">
        <base href="<?php Page::HomePage(); ?>" />
	</head>
	<body>

		<div class="container" ng-controller="LoginCtrl">

            <div class="hero-unit">
                <h1>CMStastic Panel</h1>
                <small>
                    <em>Pssst! The username is <strong>admin</strong> and the password is <strong>password</strong>...</em>
                </small>
            </div>
            
            <form name="login">

                <fieldset>
                    <legend>Please login to continue</legend>

                    <!-- Alerts -->
                    <alert-container></alert-container>

                    <label>Username</label>
                    <input type="text" placeholder="Username" ng-model="loginDetails.username" />

                    <label>Password</label>
                    <input type="password" placeholder="Password" ng-model="loginDetails.password" />

                    <div class="clearfix">
                        <button class="btn btn-primary" ng-click="submitLogin()">Sign in</button>
                    </div>

                </fieldset>

            </form>

            <div id="footer">&copy;2013 CMStastic</div>

        </div>
    
        <!-- Scripts -->
        <script src="<?php Page::HomePage(); ?>js/vendor/angular.js"></script>
        <script src="<?php Page::HomePage(); ?>js/lib/angular-resource.js"></script>
        <script src="<?php Page::HomePage(); ?>js/app.js"></script>
        <script src="<?php Page::HomePage(); ?>js/services.js"></script>
        <script src="<?php Page::HomePage(); ?>js/controllers.js"></script>
        <script src="<?php Page::HomePage(); ?>js/filters.js"></script>
        <script src="<?php Page::HomePage(); ?>js/directives.js"></script>
        <script src="<?php Page::HomePage(); ?>js/ui.bootstrap.js"></script>


	</body>
</html>