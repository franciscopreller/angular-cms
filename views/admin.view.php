<!DOCTYPE html>
<html ng-app="adminApp">
	<head>
		<title>CMS Test</title>
		<link rel="stylesheet" type="text/css" href="<?php Page::HomePage(); ?>css/style.css">
        <base href="<?php Page::HomePage(); ?>" />

        <!-- Load scripts on head - Rather have it wait before everything
             loads before displaying, rather than hang while it loads
             the menu dynamically... -->

        <script src="<?php Page::HomePage(); ?>js/vendor/angular.js"></script>
        <script src="<?php Page::HomePage(); ?>js/lib/angular-resource.js"></script>
        <script src="<?php Page::HomePage(); ?>js/app.js"></script>
        <script src="<?php Page::HomePage(); ?>js/services.js"></script>
        <script src="<?php Page::HomePage(); ?>js/controllers.js"></script>
        <script src="<?php Page::HomePage(); ?>js/filters.js"></script>
        <script src="<?php Page::HomePage(); ?>js/directives.js"></script>
        <script src="<?php Page::HomePage(); ?>js/ui.bootstrap.js"></script>
	</head>
	<body>
        
        <!-- Show the bootstrap navigation -->
        <admin-nav></admin-nav>

		<div class="container">
            <div id="main" class="row clearfix" ng-view></div>
            <div id="footer">&copy;2013 CMStastic</div>
        </div>

	</body>
</html>