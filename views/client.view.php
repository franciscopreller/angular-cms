<!DOCTYPE html>
<html data-ng-app="cmsApp">
	<head>
		<title>CMS Test</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>

    <!-- Load scripts here, to prevent the menu lagging behind when showing -->
    <script src="<?php Page::HomePage(); ?>js/vendor/angular.js"></script>
    <script src="<?php Page::HomePage(); ?>js/app.js"></script>
    <script src="<?php Page::HomePage(); ?>js/services.js"></script>
    <script src="<?php Page::HomePage(); ?>js/controllers.js"></script>
    <script src="<?php Page::HomePage(); ?>js/filters.js"></script>
    <script src="<?php Page::HomePage(); ?>js/directives.js"></script>
    <script src="<?php Page::HomePage(); ?>js/ui.bootstrap.js"></script>

	<body>
        
        <!-- Show the bootstrap navigation -->
        <bootstrap-nav></bootstrap-nav>

		<div class="container">

            <!-- Start header -->
            <header class="row">
                <section class="span2">
                    <a href="#" class="pull-left">
                        <img src="img/ABCLogo.png" width="150" height="100" alt="Company Logo" />
                    </a>
                </section>
                <section class="span10">
                    <h1><?php echo Setting::GetSettingValue('SiteTitle'); ?></h1>
                    <p id="tagline"><?php echo Setting::GetSettingValue('SiteTagline'); ?></p>
                </section>
            </header>

            <div id="main" class="clearFix">
                <?php include $page->GetLayoutFileName(); ?>
            </div>
            <div id="footer">
                <p><?php echo Setting::GetSettingValue('CopyrightText'); ?></p>
            </div>

        </div>

	</body>
</html>